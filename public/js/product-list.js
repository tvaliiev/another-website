$(function () {
    initFilters(['filter_prints', 'filter_phone_models']);
});

function initFilters(nameArray) {
    nameArray.forEach(name => {
        $('#' + name).click(function () {
            toggleFilter(name)
        });
        toggleFilter(name);
    });
}

function toggleFilter(name) {
    if ($('#' + name).is(':checked')) {
        $('.' + name + '_additional_filter').show();
    } else {
        $('.' + name + '_additional_filter').hide();
    }
}

function addToCart(dataElement) {
    const $dataEl = $(dataElement);
    const itemId = $dataEl.data('item-id');
    const amount = parseInt($dataEl.data('add-amount'));

    let items = {};
    items[itemId] = amount;

    $.post('/cart', {items}, response => {
        console.log(response);
        window.location.reload();
    });
}

function removeCartItem(dataElement) {
    const $dataEl = $(dataElement);
    const itemId = $dataEl.data('item-id');

    $.ajax({
        url: '/cart/' + itemId,
        type: 'DELETE',
        success: function (result) {
            console.log(result);
            window.location.reload();
        }
    });
}
