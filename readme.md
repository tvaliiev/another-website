## About
This is standard laravel project with no additional dependencies

## Bringing it up

Via docker and docker-compose:
1. Copy .env.example to .env
2. docker-compose up -d
3. docker-compose exec app composer install
4. docker-compose exec nodejs npm install
5. docker-compose exec nodejs npm run dev
6. docker-compose exec app ./artisan migrate:fresh --seed

That will also generate sample data to test filter. If no data required, you can just initialize new admin user:
docker-compose exec app ./artisan migrate:fresh
docker-compose exec app ./artisan db:seed --class=UsersTableSeeder

Both of these will generate admin user with admin@example.com/password credentials
