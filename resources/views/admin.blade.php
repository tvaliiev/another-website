@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3>Create product</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <form action="/admin/product" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="product_name">New product name</label>
                        <input type="text" class="form-control" id="product_name" name="product[name]">
                    </div>
                    <div class="form-group">
                        <label for="product_inventory_number">New product inventory number</label>
                        <input type="text" class="form-control" id="product_inventory_number"
                               name="product[inventory_number]">
                    </div>
                    <div class="form-group">
                        <label for="product_type">New product type:</label><br>
                        <select name="product[item_type]" id="product_type">
                            @foreach(\App\Models\Product::TYPES as $type)
                                <option value="{{$type}}">{{__('product.product_type_'.$type)}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="product_type_prints product_types_filter">
                        <div class="form-group">
                            <label for="product_print_ids">Product print</label><br>
                            <select name="product[item_id]" id="product_print_ids">
                                @foreach($prints as $print)
                                    <option value="{{$print->id}}">{{$print->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="product_print_collection">Product print collection</label><br>
                            <select name="product[item][collection][id]" id="product_item_collection_id">
                                @foreach($printCollections as $printCollection)
                                    <option value="{{$printCollection->id}}">{{$printCollection->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="product_type_phone_model product_types_filter">
                        <div class="form-group">
                            <label for="product_phone_models_ids">Phone model</label><br>
                            <select name="product[item_id]" id="product_phone_models_ids">
                                @foreach($phoneModels as $phoneModel)
                                    <option value="{{$phoneModel->id}}">{{$phoneModel->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="product_item_phone_mark_id">Phone mark</label><br>
                            <select name="product[item][phone_mark][id]" id="product_item_phone_mark_id">
                                @foreach($phoneMarks as $phoneMark)
                                    <option value="{{$phoneMark->id}}">{{$phoneMark->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <button class="btn btn-success">Submit</button>
                </form>
            </div>
            <div class="col-md-6">
                <div class="print-section">
                    <div class="create_print_form">
                        <h4>Create print</h4>
                        <form action="/admin/prints" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="print_name">New print name</label>
                                <input type="text" class="form-control" id="print_name" name="print[name]">
                            </div>
                            <div class="form-group">
                                <label for="print_size">New print size</label>
                                <select name="print[size]" id="print_size">
                                    @foreach(\App\Models\Prints::SIZES as $size)
                                        <option value="{{$size}}">{{$size}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" class="btn btn-success">Create new print</button>
                        </form>
                    </div>
                    <div class="create_print_collection_form mt-3">
                        <h4>Create print collection</h4>
                        <form action="/admin/print-collection" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="print_collection_name">New print collection name</label>
                                <input type="text" class="form-control" name="prints_collection[name]"
                                       id="print_collection_name">
                            </div>
                            <div class="form-group">
                                <label for="print_collection_create_year">New print collection create year</label>
                                <input type="text" class="form-control" name="prints_collection[create_year]"
                                       id="print_collection_name">
                            </div>
                            <div class="form-group">
                                <label for="print_collection_create_year">New print collection description</label>
                                <textarea type="text" class="form-control" name="prints_collection[description]"
                                          id="print_collection_description" rows="5"></textarea>
                            </div>
                            <button class="btn btn-success">Create new print collection</button>
                        </form>
                    </div>
                </div>
                <div class="phone-mark-section">
                    <div class="create_phone_model mt-3">
                        <h4>Create new phone model</h4>
                        <form action="/admin/phone-models" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="phone_model_name">
                                    New phone model name
                                </label>
                                <input type="text" class="form-control" name="phone_model[name]" id="phone_model_name">
                            </div>
                            <button class="btn btn-success">Create new phone model</button>
                        </form>
                    </div>
                    <div class="create_phone_mark mt-3">
                        <h4>Create new phone mark</h4>
                        <form action="/admin/phone-marks" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="phone_mark_name">
                                    New phone mark name
                                </label>
                                <input type="text" class="form-control" name="phone_mark[name]" id="phone_mark_name">
                            </div>
                            <div class="form-group">
                                <label for="phone_mark_code">
                                    New phone mark code
                                </label>
                                <input type="number" class="form-control" name="phone_mark[code]" id="phone_mark_code">
                            </div>
                            <div class="form-group">
                                <label for="phone_mark_description">
                                    New phone mark description
                                </label>
                                <textarea type="text" class="form-control" name="phone_mark[description]"
                                          id="phone_mark_description"></textarea>
                            </div>
                            <button class="btn btn-success">Create new phone mark</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/admin.js')}}"></script>
@endpush
