@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-3">
                <form action="/" method="GET">
                    <div class="form-group">
                        <p>Filter</p>
                        <hr>
                    </div>
                    <div class="form-group">
                        <label for="filter_product_name">
                            Product name:
                        </label><br>
                        <input type="text" id="filter_product_name" name="filter[name]"
                               value="{{$filter['name'] ?? ''}}">
                    </div>
                    <div class="form-group">
                        <label for="filter_prints">
                            <input type="checkbox" value="{{\App\Models\Product::TYPE_PRINTS}}"
                                   name="filter[types][]"
                                   id="filter_prints" {{in_array(\App\Models\Product::TYPE_PRINTS, $filter['types']) ? 'checked' : ''}}
                            >
                            {{__('product.product_type_'.\App\Models\Product::TYPE_PRINTS)}}
                            <div class="filter_prints_additional_filter ml-3">
                                <div class="form-group">
                                    <label for="filter_prints_ids">Prints (multiple selection allowed):</label><br>
                                    <select name="filter[prints][ids][]" id="filter_prints_ids" multiple
                                            style="max-width: 300px;">
                                        @foreach($prints as $print)
                                            <option value="{{$print->id}}"
                                                {{isset($filter['prints']['ids']) && in_array($print->id, $filter['prints']['ids']) ? 'selected' : ''}}>
                                                {{$print->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="filter_prints_collections">Collection (multiple selection
                                        allowed):</label><br>
                                    <select name="filter[prints][collections][]" id="filter_prints_collections" multiple
                                            style="max-width: 300px;">
                                        @foreach($printCollections as $collection)
                                            <option value="{{$collection->id}}"
                                                {{isset($filter['prints']['collections']) && in_array($collection->id, $filter['prints']['collections']) ? 'selected' : ''}}>
                                                {{$collection->name}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="filter_prints_collections">Print size (multiple selection
                                        allowed):</label><br>
                                    <select name="filter[prints][sizes][]" id="filter_prints_collections" multiple
                                            style="max-width: 300px;">
                                        @foreach(\App\Models\Prints::SIZES as $size)
                                            <option value="{{$size}}"
                                                {{isset($filter['prints']['sizes']) && in_array($size, $filter['prints']['sizes']) ? 'selected' : ''}}>
                                                {{$size}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="form-group">

                        <label class="form-check-label" for="filter_phone_models">
                            <input type="checkbox"
                                   value="{{\App\Models\Product::TYPE_PHONE_MODEL}}" id="filter_phone_models"
                                   name="filter[types][]"
                                {{in_array(\App\Models\Product::TYPE_PHONE_MODEL, $filter['types'] ?? []) ? 'checked' : ''}}
                            >
                            {{__('product.product_type_'.\App\Models\Product::TYPE_PHONE_MODEL)}}
                            <div class="filter_phone_models_additional_filter ml-3">
                                <div class="form-group">
                                    <label for="phone_model_name">Phone model (multiple selection allowed):</label><br>
                                    <select name="filter[phone_models][ids][]" id="phone_model_marks" multiple
                                            style="max-width: 300px;">
                                        @foreach($phoneModels as $phoneModel)
                                            <option value="{{$phoneModel->id}}"
                                                {{in_array($phoneModel->id, $filter['phone_models']['ids'] ?? []) ? 'selected':''}}
                                            >{{$phoneModel->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Phone mark:</label><br>
                                    <select name="filter[phone_models][marks][ids][]" id="phone_model_marks" multiple
                                            style="max-width: 300px;">
                                        @foreach($phoneMarks as $phoneMark)
                                            <option value="{{$phoneMark->id}}"
                                                {{in_array($phoneMark->id, $filter['phone_models']['marks']['ids'] ?? []) ? 'selected':''}}
                                            >{{$phoneMark->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </label>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-outline-secondary" href="/">Reset filter</a>
                </form>
            </div>
            <div class="col-md-6">
                {{$products->appends(['filter' => $filter])->links()}}
                <div class="row mb-4">
                    @foreach($products as $product)
                        @if($loop->index % 3 === 0)
                </div>
                <div class="row mb-3">
                    @endif
                    <div class="col-md-3">
                        <div class="card">
                            <img class="card-img-top" src="https://via.placeholder.com/150" alt="Card image cap">
                            <div class="card-body">
                                Product ID: {{$product->id}} <br>
                                Product name: {{$product->name}} <br>
                                @if($product->item_type === \App\Models\Product::TYPE_PRINTS)
                                    Size: {{$product->item->size}}<br>
                                @elseif($product->item_type === \App\Models\Product::TYPE_PHONE_MODEL)
                                    Phone model name: {{$product->item->name}}<br>
                                    Phone mark: {{$product->item->mark->name}}<br>
                                @endif
                                Type: <a href="/?filter[types][]={{$product->item_type}}">{{$product->item_type}}</a>
                                <br>
                                Description: {{$product->description}}<br>
                                <a href="#" data-item-id="{{$product->id}}" data-add-amount="1"
                                   onclick="addToCart(this); return false;" class="btn btn-outline-info">Add to cart</a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{$products->appends(['filter' => $filter])->links()}}
            </div>
            <div class="col-md-2">
                Your cart:
                @foreach($cart as $product)
                    <div class="row">
                        <div class="col-md-12">
                            <p>
                                {{$product->item->name}}
                            </p>
                            <a href="#" data-item-id="{{$product->item->id}}"
                               onclick="removeCartItem(this); return false;" class="btn btn-danger">X</a>
                        </div>
                    </div>
                @endforeach
                <a href="/order" class="btn btn-success mt-5">Order</a>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/product-list.js')}}" defer></script>
@endpush
