@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="mb-5">Your order</h1>
                <div>
                    <p>Your cart:</p>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product name</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cart as $cartItem)
                            <tr>
                                <th scope="row">{{$loop->index}}</th>
                                <td>{{$cartItem->item->name}}</td>
                                <td>{{$cartItem->amount}}</td>
                                <td><a href="#" data-item-id="{{$cartItem->item->id}}" onclick="removeCartItem(this)"
                                       class="btn btn-danger">X</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div>
                    <p>Email address:</p>
                    <input type="text" name="email" value="{{Auth::user()->email ?? ''}}">
                </div>
                <div>
                    <p>Address:</p>
                    <textarea name="address">{{Auth::user()->address ?? ''}}</textarea>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{asset('js/order.js')}}"></script>
@endpush
