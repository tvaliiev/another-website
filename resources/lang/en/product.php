<?php

use App\Models\Product;

return [
    'product_type_' . Product::TYPE_PRINTS => 'Print',
    'product_type_' . Product::TYPE_PHONE_MODEL => 'Phone model',
];
