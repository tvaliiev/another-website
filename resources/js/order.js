function removeCartItem(dataElement) {
    const $dataEl = $(dataElement);
    const itemId = $dataEl.data('item-id');

    $.ajax({
        url: '/cart/' + itemId,
        type: 'DELETE',
        success: function (result) {
            $dataEl.parent().parent().detach();
        }
    });
}
