$('#product_type')
    .on('change', onProductTypeChange)
    .change();

function onProductTypeChange() {
    $('.product_types_filter').hide();
    $('.product_type_' + this.value).show();
}
