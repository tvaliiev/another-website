<?php


namespace App\Repositories\Contracts;


use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface ProductRepositoryContract
{
    /**
     * @param array $filter
     * @return \Illuminate\Support\Collection
     */
    public function getList(array $filter): Collection;

    /**
     * @param array $filter
     * @param int|null $perPage
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getListPaginated(array $filter, ?int $perPage): LengthAwarePaginator;
}
