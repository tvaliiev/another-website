<?php

namespace App\Repositories\Contracts;

use Illuminate\Support\Collection;

/**
 * Interface CartRepositoryContract
 * @package App\Repositories\Interfaces
 */
interface CartRepositoryContract
{
    /**
     * @param array $items Should be array where key is item id and value is amount of items
     */
    public function addToCart(array $items): void;


    /**
     * @param array $items array of ids which remove from cart
     */
    public function removeFromCart(array $items): void;

    /**
     * @param array $cartData Should be array where key is item id and value is amount of items
     */
    public function saveCart(array $cartData): void;

    /**
     * @return Collection Cart data, where key is item id and value is amount of items
     */
    public function getCart(): Collection;
}
