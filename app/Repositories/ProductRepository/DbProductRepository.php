<?php


namespace App\Repositories\ProductRepository;


use App\Models\Product;
use App\Repositories\Contracts\ProductRepositoryContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class DbProductRepository implements ProductRepositoryContract
{
    /**
     * @param array $filter
     * @return \Illuminate\Support\Collection
     */
    public function getList(array $filter): Collection
    {
        $products = Product::query();

        $this->filter($filter, $products);

        return $products->get();
    }

    /**
     * @param array $filter
     * @param $products
     */
    private function filter(array $filter, Builder $products): void
    {
        $products->with('item');

        if (isset($filter['types'])) {
            $products->whereIn('item_type', $filter['types']);
        }

        $products->where(static function ($products) use ($filter) {
            if (isset($filter['prints'])) {
                $products->with('print');
                $products->whereHas('print', static function (Builder $queryPrint) use ($filter) {
                    if (isset($filter['prints']['ids'])) {
                        $queryPrint->whereIn('id', $filter['prints']['ids']);
                    }

                    if (isset($filter['prints']['sizes'])) {
                        $queryPrint->whereIn('size', $filter['prints']['sizes']);
                    }

                    if (isset($filter['prints']['collections'])) {
                        $queryPrint->with('collections');
                        $queryPrint->whereHas('collections', static function (Builder $queryProductCollection) use ($filter) {
                            $queryProductCollection->whereIn('id', $filter['prints']['collections']);
                        });
                    }
                });
            }

            if (isset($filter['phone_models'])) {
                $products->with('phoneModel');
                $products->orWhereHas('phoneModel', static function (Builder $queryPhoneModels) use ($filter) {
                    if (isset($filter['phone_models']['ids'])) {
                        $queryPhoneModels->whereIn('id', $filter['phone_models']['ids']);
                    }

                    if (isset($filter['phone_models']['name'])) {
                        $queryPhoneModels->where('name', 'LIKE', '%' . $filter['phone_models']['name'] . '%');
                    }

                    if (isset($filter['phone_models']['marks']['ids'])) {
                        $queryPhoneModels->whereIn('phone_mark_id', $filter['phone_models']['marks']);
                    }
                });
            }
        });

        if (isset($filter['name'])) {
            $products->where('name', 'LIKE', '%' . $filter['name'] . '%');
        }

        if (isset($filter['ids'])) {
            $products->whereIn('id', $filter['ids']);
        }
    }

    public function getListPaginated(array $filter, ?int $perPage = null): LengthAwarePaginator
    {
        $products = Product::query();

        $this->filter($filter, $products);

        return $products->paginate($perPage);
    }


}
