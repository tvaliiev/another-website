<?php


namespace App\Repositories\CartRepository;


use App\Models\Product;
use App\Repositories\Contracts\CartRepositoryContract;
use App\Repositories\Contracts\ProductRepositoryContract;
use Illuminate\Support\Collection;

class SessionCartRepository implements CartRepositoryContract
{
    private $productRepository;

    public function __construct(ProductRepositoryContract $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function addToCart(array $items): void
    {
        $cart = session('cart', []);

        array_walk($items, static function ($v, $k) use (&$cart) {
            $cart[$k] = $v;
        });

        session(['cart' => $cart]);
    }

    public function removeFromCart(array $items): void
    {
        $cart = session('cart', []);

        foreach ($items as $item) {
            unset($cart[$item]);
        }

        session(['cart' => $cart]);
    }

    public function saveCart(array $cartData): void
    {
        session(['cart' => $cartData]);
    }

    public function getCart(): Collection
    {
        $cartData = session('cart', []);

        if (empty($cartData)) {
            return collect();
        }

        return $this->productRepository
            ->getList(['ids' => array_keys($cartData)])
            ->map(static function (Product $product) use ($cartData) {
                return (object)[
                    'item' => $product,
                    'amount' => $cartData[$product->id],
                ];
            });
    }
}
