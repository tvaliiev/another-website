<?php


namespace App\Http\Controllers;


use App\Repositories\Contracts\CartRepositoryContract;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Repositories\Contracts\CartRepositoryContract $cartRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToCart(Request $request, CartRepositoryContract $cartRepository): JsonResponse
    {
        $cartRepository->addToCart($request->input('items', []));

        return response()->json(['success' => true]);
    }

    /**
     * @param $id int Product id which to delete from cart
     * @param \App\Repositories\Contracts\CartRepositoryContract $cartRepository
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeFromCart($id, CartRepositoryContract $cartRepository): JsonResponse
    {
        $cartRepository->removeFromCart([$id]);

        return response()->json(['success' => true]);
    }
}
