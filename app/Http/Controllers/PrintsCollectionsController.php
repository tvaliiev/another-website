<?php


namespace App\Http\Controllers;


use App\Models\PrintCollection;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PrintsCollectionsController extends Controller
{
    public function addNewPrintsCollection(Request $request): RedirectResponse
    {
        $newPrintCollectionData = $this->validate($request, [
            'prints_collection.name' => ['string'],
            'prints_collection.create_year' => ['nullable', 'numeric'],
            'prints_collection.description' => ['nullable', 'string'],
        ]);

        $printCollection = new PrintCollection();
        $printCollection->fill($newPrintCollectionData['prints_collection']);
        $printCollection->save();

        return response()->redirectTo('/admin');
    }
}
