<?php


namespace App\Http\Controllers;


use App\Models\PhoneModel;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PhoneModelsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addNewPhoneModel(Request $request): RedirectResponse
    {
        $phoneModelData = $this->validate($request, [
            'phone_model.name' => ['string'],
        ]);
        $phoneModelData = $phoneModelData['phone_model'];

        $phoneModel = new PhoneModel();
        $phoneModel->fill($phoneModelData);
        $phoneModel->save();

        return response()->redirectTo('/admin');
    }
}
