<?php


namespace App\Http\Controllers;


use App\Models\PhoneMark;
use App\Models\PhoneModel;
use App\Models\PrintCollection;
use App\Models\Prints;
use App\Models\Product;
use App\Repositories\Contracts\CartRepositoryContract;
use App\Repositories\Contracts\ProductRepositoryContract;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class ProductsController extends Controller
{
    public const PRODUCTS_PER_PAGE = 15;

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Repositories\Contracts\ProductRepositoryContract $productRepository
     * @param \App\Repositories\Contracts\CartRepositoryContract $cartRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request, ProductRepositoryContract $productRepository, CartRepositoryContract $cartRepository)
    {
        try {
            $request = $this->validate($request, [
                'filter.types.*' => [Rule::in(Product::TYPES)],
                'filter.phone_models.name' => [],
                'filter.phone_models.marks' => [],
                'filter.phone_models.marks.ids' => [],
                'filter.phone_models.ids' => [],
                'filter.prints.*' => [],
                'filter.name' => [],
                'filter.inventory_number' => [],
            ]);
        } catch (ValidationException $e) {
            // Remove filter
            return redirect('/');
        }

        $filter = $request['filter'] ?? [];

        if (isset($filter['types']) && !in_array(Product::TYPE_PRINTS, $filter['types'])) {
            unset($filter['prints']);
        }
        if (isset($filter['types']) && !in_array(Product::TYPE_PHONE_MODEL, $filter['types'])) {
            unset($filter['phone_models']);
        }

        $products = $productRepository->getListPaginated($filter, self::PRODUCTS_PER_PAGE);

        // Default values for view
        $filter['types'] = $filter['types'] ?? Product::TYPES;

        $printCollections = PrintCollection::all();
        $phoneMarks = PhoneMark::all();
        $cart = $cartRepository->getCart();
        $prints = Prints::all();
        $phoneModels = PhoneModel::all();


        return view('products.list', compact(
            'products',
            'filter',
            'printCollections',
            'phoneMarks',
            'cart',
            'prints',
            'phoneModels'
        ));
    }

    public function addNewProduct(Request $request)
    {
        $productData = $this->validate($request, [
            'product.name' => ['string'],
            'product.inventory_number' => ['nullable', 'string'],
            'product.item_type' => [Rule::in(Product::TYPES)],
            'product.item_id' => ['numeric'],
            'product.item.collection.id' => ['numeric'],
            'product.item.phone_mark.id' => ['numeric'],
        ]);
        $productData = $productData['product'];

        $product = new Product();
        $product->fill([
            'name' => $productData['name'],
            'inventory_number' => $productData['inventory_number'],
        ]);

        $product->item_type = $productData['item_type'];
        $product->item_id = $productData['item_id'];
        $product->save();

        if (
            $product->item_type === Product::TYPE_PRINTS &&
            PrintCollection::whereId($productData['item']['collection']['id'])->exists()
        ) {
            $product->print->collections()->attach($productData['item']['collection']['id']);
        } else if (
            $product->item_type === Product::TYPE_PHONE_MODEL &&
            PhoneMark::whereId($productData['item']['phone_mark']['id'])->exists()
        ) {
            $product->phoneModel->phone_mark_id = $productData['item']['phone_mark']['id'];
            $product->phoneModel->save();
        }

        return response()->redirectTo('/admin');
    }
}
