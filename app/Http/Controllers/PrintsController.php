<?php


namespace App\Http\Controllers;


use App\Models\Prints;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PrintsController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addNewPrint(Request $request): RedirectResponse
    {
        $printData = $this->validate($request, [
            'print.size' => ['string', Rule::in(Prints::SIZES)],
            'print.name' => ['string', 'min:2'],
        ]);

        $prints = new Prints();
        $prints->fill($printData['print']);
        $prints->save();

        return response()->redirectTo('/admin');
    }
}
