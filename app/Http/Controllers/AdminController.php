<?php


namespace App\Http\Controllers;


use App\Models\PhoneMark;
use App\Models\PhoneModel;
use App\Models\PrintCollection;
use App\Models\Prints;

class AdminController extends Controller
{
    public function admin()
    {
        $prints = Prints::all();
        $phoneModels = PhoneModel::all();
        $phoneMarks = PhoneMark::all();
        $printCollections = PrintCollection::all();

        return view('admin', compact('prints', 'phoneModels', 'phoneMarks', 'printCollections'));
    }
}
