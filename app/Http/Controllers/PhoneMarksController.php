<?php


namespace App\Http\Controllers;


use App\Models\PhoneMark;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PhoneMarksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function addNewPhoneMark(Request $request): RedirectResponse
    {
        $phoneMarkData = $this->validate($request, [
            'phone_mark.name' => ['string'],
            'phone_mark.code' => ['nullable', 'numeric'],
            'phone_mark.description' => ['nullable', 'string'],
        ]);
        $phoneMarkData = $phoneMarkData['phone_mark'];

        $phoneMark = new PhoneMark();
        $phoneMark->fill($phoneMarkData);
        $phoneMark->save();

        return redirect('/admin');
    }
}
