<?php


namespace App\Http\Controllers;


use App\Repositories\Contracts\CartRepositoryContract;

class OrderController extends Controller
{
    public function order(CartRepositoryContract $cartRepository)
    {
        $cart = $cartRepository->getCart();
        return view('order.order', compact('cart'));
    }
}
