<?php

namespace App\Providers;

use App\Repositories\CartRepository\SessionCartRepository;
use App\Repositories\Contracts\CartRepositoryContract;
use App\Repositories\Contracts\ProductRepositoryContract;
use App\Repositories\ProductRepository\DbProductRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    public $bindings = [
        ProductRepositoryContract::class => DbProductRepository::class,
        CartRepositoryContract::class => SessionCartRepository::class,
    ];
}
