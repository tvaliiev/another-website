<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * App\Models\PhoneModel
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property int $phone_mark_id
 * @property-read \App\Models\PhoneMark $mark
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @method static Builder|PhoneModel newModelQuery()
 * @method static Builder|PhoneModel newQuery()
 * @method static Builder|PhoneModel query()
 * @method static Builder|PhoneModel whereCreatedAt($value)
 * @method static Builder|PhoneModel whereId($value)
 * @method static Builder|PhoneModel whereName($value)
 * @method static Builder|PhoneModel wherePhoneMarkId($value)
 * @method static Builder|PhoneModel whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PhoneModel extends Model
{
    protected $fillable = [
        'name',
    ];

    public function products(): MorphMany
    {
        return $this->morphMany(Product::class, 'item');
    }

    public function mark(): BelongsTo
    {
        return $this->belongsTo(PhoneMark::class, 'phone_mark_id');
    }
}
