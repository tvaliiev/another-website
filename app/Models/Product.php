<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $inventory_number
 * @property string|null $description
 * @property string|null $name
 * @property int $item_id
 * @property string $item_type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $item
 * @property-read \App\Models\PhoneModel $phoneModel
 * @property-read \App\Models\Prints $print
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereDescription($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereInventoryNumber($value)
 * @method static Builder|Product whereItemId($value)
 * @method static Builder|Product whereItemType($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    protected $fillable = [
        'inventory_number', 'name',
    ];

    public const TYPE_PRINTS = 'prints';
    public const TYPE_PHONE_MODEL = 'phone_model';
    public const TYPES = [
        self::TYPE_PRINTS,
        self::TYPE_PHONE_MODEL,
    ];

    public function item(): MorphTo
    {
        return $this->morphTo();
    }

    public function print(): BelongsTo
    {
        return $this
            ->belongsTo(Prints::class, 'item_id');
    }

    public function phoneModel(): BelongsTo
    {
        return $this
            ->belongsTo(PhoneModel::class, 'item_id');
    }
}
