<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * App\Models\Prints
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $name
 * @property string|null $size
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PrintCollection[] $collections
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Prints whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Prints extends Model
{
    protected $fillable = [
        'size', 'name',
    ];

    public const SIZES = [
        'S', 'M', 'XL', 'XXL',
    ];
    protected $table = 'prints';

    public function products(): MorphMany
    {
        return $this->morphMany(Product::class, 'item');
    }

    public function collections(): BelongsToMany
    {
        return $this->belongsToMany(PrintCollection::class, 'print_to_print_collection', 'print_id');
    }
}
