<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PhoneMark
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property string|null $code
 * @property string|null $description
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\PhoneModel[] $phones
 * @method static Builder|PhoneMark newModelQuery()
 * @method static Builder|PhoneMark newQuery()
 * @method static Builder|PhoneMark query()
 * @method static Builder|PhoneMark whereCode($value)
 * @method static Builder|PhoneMark whereCreatedAt($value)
 * @method static Builder|PhoneMark whereDescription($value)
 * @method static Builder|PhoneMark whereId($value)
 * @method static Builder|PhoneMark whereName($value)
 * @method static Builder|PhoneMark whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PhoneMark extends Model
{
    protected $fillable = [
        'name', 'code', 'description',
    ];

    public function phones()
    {
        return $this->hasMany(PhoneModel::class);
    }
}
