<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PrintCollection
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $name
 * @property int|null $create_year
 * @property string|null $description
 * @method static Builder|PrintCollection newModelQuery()
 * @method static Builder|PrintCollection newQuery()
 * @method static Builder|PrintCollection query()
 * @method static Builder|PrintCollection whereCreateYear($value)
 * @method static Builder|PrintCollection whereCreatedAt($value)
 * @method static Builder|PrintCollection whereDescription($value)
 * @method static Builder|PrintCollection whereId($value)
 * @method static Builder|PrintCollection whereName($value)
 * @method static Builder|PrintCollection whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class PrintCollection extends Model
{
    protected $fillable = [
        'name', 'create_year', 'description',
    ];
}
