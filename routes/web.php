<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ProductsController@list');
Route::post('/cart', 'CartController@addToCart');
Route::delete('/cart/{id}', 'CartController@removeFromCart');
Route::get('/order', 'OrderController@order');

Route::get('/admin', 'AdminController@admin');
Route::post('/admin/product', 'ProductsController@addNewProduct');

Route::get('/admin/prints', 'PrintsController@showPrints');
Route::post('/admin/prints', 'PrintsController@addNewPrint');

Route::post('/admin/print-collection', 'PrintsCollectionsController@addNewPrintsCollection');

Route::post('/admin/phone-models', 'PhoneModelsController@addNewPhoneModel');

Route::post('/admin/phone-marks', 'PhoneMarksController@addNewPhoneMark');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
