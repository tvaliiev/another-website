<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInitialTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('products', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('inventory_number')->nullable();
            $table->text('description')->nullable();
            $table->string('name')->nullable();

            $table->unsignedBigInteger('item_id');
            $table->string('item_type');
        });

        Schema::create('prints', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name')->nullable();
            $table->string('size')->nullable();
        });

        Schema::create('print_collections', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->integer('create_year')->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('print_to_print_collection', static function (Blueprint $table) {
            $table->unsignedBigInteger('print_id');
            $table->unsignedBigInteger('print_collection_id');

            $table->foreign('print_id')
                ->references('id')->on('prints')
                ->onDelete('cascade');
            $table->foreign('print_collection_id')
                ->references('id')->on('print_collections')
                ->onDelete('cascade');
        });

        Schema::create('phone_marks', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');
            $table->string('code')->nullable();
            $table->text('description')->nullable();
        });

        Schema::create('phone_models', static function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('name');

            $table->unsignedBigInteger('phone_mark_id')->nullable();
            $table->foreign('phone_mark_id')
                ->references('id')->on('phone_marks')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
}
