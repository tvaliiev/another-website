<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PrintsTableSeeder::class);
        $this->call(PrintCollectionsTableSeeder::class);
        $this->call(PhoneMarksSeeder::class);
        $this->call(PhoneModelsSeeder::class);
    }
}
