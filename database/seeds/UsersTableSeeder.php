<?php


use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    public function run(): void
    {
        factory(User::class)->create([
            'email' => 'admin@example.com',
            'admin_rights' => 1,
        ]);
    }
}
