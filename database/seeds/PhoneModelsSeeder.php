<?php


use App\Models\PhoneMark;
use App\Models\PhoneModel;
use App\Models\Product;
use Illuminate\Database\Seeder;

class PhoneModelsSeeder extends Seeder
{

    public function run(): void
    {
        PhoneMark::all()->each(static function (PhoneMark $phoneMark) {
            factory(PhoneModel::class)
                ->times(random_int(5, 9))
                ->create(['phone_mark_id' => $phoneMark->id])
                ->each(static function (PhoneModel $phoneModel) {
                    $phoneModel->products()->saveMany(
                        factory(Product::class)->times(random_int(2, 3))->make()
                    );
                });
        });
    }
}
