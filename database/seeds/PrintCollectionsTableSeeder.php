<?php


use App\Models\PrintCollection;
use App\Models\Prints;
use Illuminate\Database\Seeder;

class PrintCollectionsTableSeeder extends Seeder
{

    public function run(): void
    {
        $prints = Prints::all();
        factory(PrintCollection::class)
            ->times(random_int(3, 5))
            ->create()
            ->each(static function (PrintCollection $printCollection) use ($prints) {
                $prints->random(random_int(2, 5))->each(static function (Prints $print) use ($printCollection) {
                    $print
                        ->collections()
                        ->attach($printCollection);

                });
            });
    }
}
