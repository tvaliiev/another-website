<?php


use App\Models\Prints;
use App\Models\Product;
use Illuminate\Database\Seeder;

class PrintsTableSeeder extends Seeder
{

    public function run(): void
    {
        factory(Prints::class)
            ->times(random_int(5, 15))
            ->create()
            ->each(static function (Prints $print) {
                $products = factory(Product::class)
                    ->times(random_int(5, 10))
                    ->make();
                $print->products()->saveMany($products);
            });
    }
}
