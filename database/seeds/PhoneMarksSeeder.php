<?php


use App\Models\PhoneMark;
use Illuminate\Database\Seeder;

class PhoneMarksSeeder extends Seeder
{

    public function run(): void
    {
        factory(PhoneMark::class)
            ->times(random_int(4, 12))
            ->create();
    }
}
