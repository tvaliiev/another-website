<?php

use App\Models\PhoneModel;
use Faker\Generator as Faker;

$factory->define(PhoneModel::class, static function (Faker $faker) {
    return [
        'name' => 'Phone Model "' . $faker->words(random_int(1, 5), true) . '"',
    ];
});
