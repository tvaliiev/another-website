<?php

use App\Models\PrintCollection;
use Faker\Generator as Faker;

$factory->define(PrintCollection::class, static function (Faker $faker) {
    return [
        'name' => 'Print collection "' . $faker->words(random_int(1, 5), true) . '"',
        'description' => $faker->paragraphs(random_int(1, 5), true),
        'create_year' => $faker->year,
    ];
});
