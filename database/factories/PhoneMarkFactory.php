<?php

use App\Models\PhoneMark;
use Faker\Generator as Faker;

$factory->define(PhoneMark::class, static function (Faker $faker) {
    return [
        'name' => 'Phone mark "' . $faker->words(random_int(1, 3), true) . '"',
        'description' => $faker->paragraph,
        'code' => $faker->randomLetter . $faker->randomLetter . $faker->randomLetter,
    ];
});
