<?php

use App\Models\Prints;
use Faker\Generator as Faker;

$factory->define(Prints::class, static function (Faker $faker) {
    return [
        'name' => $faker->words(random_int(1, 5), true),
        'size' => $faker->randomElement(Prints::SIZES),
    ];
});
