<?php

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, static function (Faker $faker) {
    return [
        'inventory_number' => $faker->randomLetter . $faker->randomLetter . '-' . $faker->unique()->numerify('######'),
        'description' => $faker->paragraph,
        'name' => $faker->words(random_int(2, 7), true),
    ];
});
